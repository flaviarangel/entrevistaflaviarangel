Questão 3

A aplicação está estruturada em duas pastas, são elas main e teste. A pasta main contém toda a aplicação e a pasta test contém os testes unitários relativos a ela.
Dentro da pasta main existem os pacotes entity, exception e service. 
O pacote entity contém as classes Stream e StreamEntity que representam a estrutura do dado a ser manipulado. No conceito MVC, pode-se dizer que esse pacote contém todas as classes representativas do Model. A classe StreamEntity tem um atributo array de char e um outro inteiro que representa a posição de leitura do array. Essa classe implementa a interface Stream definida no enunciado da questão, além de conter os getters e setters de seus atributos. 
Já o pacote exception contém a classe NoFirstCharFoundException que define o erro e sua mensagem que serão lançados na aplicação, quando não houver um caracter que não se repete, por todo o array de char informado.
Por fim, o pacote service contém a classe StreamService que define toda a regra de negócio(lógica) a ser usada para resolver o problema do enunciado 3. No conceito MVC, pode-se dizer que esse pacote contém todas as classes representativas do Controller. A classe StreamService implementa o método firstChar, o qual foi desenvolvido com o seguinte raciocínio:

* HashSet<Character> hashSetRepetidos            : Armazena os caracteres que se repetem. Estrutura usada, porque um Set não aceita elementos repetidos.A ordem de inclusão nessa estrutura não importa.
* LinkedHashSet<Character> linkedSetNaoRepetidos : Armazena os caracteres que não se repetem. Estrutura usada, porque um Linked armazena os elementos na ordem de inclusão e por ser um Set não aceita elementos repetidos.

Para todo caracter que for consumido da array de chars da StreamEntity, primeiramente é verifico se esse existe no array hashSetRepetidos. 
Se sim, não faz nada com o caracter, pois ele já se repetiu mais de uma vez no array de char da StreamEntity.
Se não, tenta incluí-lo no linkedSetNaoRepetidos, 
	Se essa estrutura permitir a inclusão é sinal de que esse caracter que está sendo manipulado não se repetiu até o momento.
	Se essa estrutura não permitir a inclusão é sinal que é a primeira vez que esse caracter está se repetindo, e portanto deve-se desconsidera-lo. Para isso, remove-se o caracter já existente da estrutura linkedSetNaoRepetidos e adiciona-o no hashSetRepetidos.

Ao final do método, é verificado se a estrutura linkedSetNaoRepetidos contém algum elemento
Se não, retorna o erro NoFirstCharFoundException, com a messagem amigavél "Todos os elementos do array repetem."
Se sim, retorna o primeiro elemento desse set.

Para testar todos os cenários possível dessa aplicação, foram criados testes unitários na pasta test e pacote streamtest, os quais abrangem os casos:
"Quando o array de char da StreamEntity":
1) é nula: testWhenArrayIsNull().
2) não contém elementos: testWhenArrayHasNoElements().
3) tem apenas 1 elemento: testWhenArrayHasOneElement().
4) tem todos os elementos diferente uns dos outros: testWhenArrayHasAllDiferentElements.
5) tem todos os elementos iguais e um tamanho de array ímpar: testWhenArrayHasAllTheSameElementsAndArraySizeIsOdd.
6) tem todos os elementos iguais e um tamanho de array par: testWhenArrayHasAllTheSameElementsAndArraySizeIsEven.
7) representa o exemplo dado no enunciado da questão 3: testSampleFromTheExercise.
