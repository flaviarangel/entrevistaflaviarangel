package streamtest;

import java.util.NoSuchElementException;

import org.junit.Assert;
import org.junit.Test;

import entity.StreamEntity;
import exception.NoFirstCharFoundException;
import service.StreamService;

public class StreamTest {

	@Test(expected = NoSuchElementException.class)
	public void testWhenArrayIsNull() throws NoFirstCharFoundException {
		char[] array = null;
		StreamService.firstChar(new StreamEntity(array));
	}

	@Test(expected = NoSuchElementException.class)
	public void testWhenArrayHasNoElements() throws NoFirstCharFoundException {
		char[] array = {};
		StreamService.firstChar(new StreamEntity(array));
	}

	@Test
	public void testWhenArrayHasOneElement() throws NoFirstCharFoundException {
		char[] array = { 'a' };
		makeAssert(array, 'a');
	}

	@Test
	public void testWhenArrayHasAllDiferentElements() throws NoFirstCharFoundException {
		char[] array = { 'a', 'b', 'c' };
		makeAssert(array, 'a');
	}

	@Test(expected = NoFirstCharFoundException.class)
	public void testWhenArrayHasAllTheSameElementsAndArraySizeIsOdd() throws NoFirstCharFoundException {
		char[] array = { 'a', 'a', 'a' };
		StreamService.firstChar(new StreamEntity(array));
	}

	@Test(expected = NoFirstCharFoundException.class)
	public void testWhenArrayHasAllTheSameElementsAndArraySizeIsEven() throws NoFirstCharFoundException {
		char[] array = { 'a', 'a', 'a', 'a' };
		StreamService.firstChar(new StreamEntity(array));
	}

	@Test
	public void testSampleFromTheExercise() throws NoFirstCharFoundException {
		char[] array = { 'a', 'A', 'b', 'B', 'A', 'B', 'a', 'c' };
		makeAssert(array, 'b');
	}

	private void makeAssert(char[] array, char expectedAnswer) throws NoFirstCharFoundException {
		char firstNotRepeatedChar = StreamService.firstChar(new StreamEntity(array));
		Assert.assertEquals(expectedAnswer, firstNotRepeatedChar);
	}

}
