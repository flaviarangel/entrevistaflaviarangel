package entity;

public class StreamEntity implements Stream {

	private char[] arrayChars;

	private int arrayPosition = -1;

	public StreamEntity(char[] arrayChars) {
		this.setArrayChars(arrayChars);
	}

	@Override
	public char getNext() {
		arrayPosition++;
		return arrayChars[arrayPosition];
	}

	@Override
	public boolean hasNext() {
		if ((arrayChars == null) || (arrayChars.length == 0))
			return false;

		if (arrayPosition < arrayChars.length - 1) {
			return true;
		}
		return false;
	}

	public char[] getArrayChars() {
		return arrayChars;
	}

	public void setArrayChars(char[] arrayChars) {
		this.arrayChars = arrayChars;
	}

	public int getArrayPosition() {
		return arrayPosition;
	}

	public void setArrayPosition(int arrayPosition) {
		this.arrayPosition = arrayPosition;
	}

}
