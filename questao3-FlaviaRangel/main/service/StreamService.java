package service;

import java.util.HashSet;
import java.util.LinkedHashSet;

import entity.Stream;
import exception.NoFirstCharFoundException;

public class StreamService {

	public static char firstChar(Stream input) throws NoFirstCharFoundException {

		if (!input.hasNext())
			throw new java.util.NoSuchElementException("N�o h� elementos no array");

		LinkedHashSet<Character> linkedSetNaoRepetidos = new LinkedHashSet<Character>();
		HashSet<Character> hashSetRepetidos = new HashSet<Character>();

		while (input.hasNext()) {
			char caracter = input.getNext();
			if (!hashSetRepetidos.contains(caracter)) {
				if (!linkedSetNaoRepetidos.add(caracter)) {
					linkedSetNaoRepetidos.remove(caracter);
					hashSetRepetidos.add(caracter);
				}
			}
		}

		if (linkedSetNaoRepetidos.isEmpty())
			throw new NoFirstCharFoundException();

		return linkedSetNaoRepetidos.iterator().next();
	}

}
