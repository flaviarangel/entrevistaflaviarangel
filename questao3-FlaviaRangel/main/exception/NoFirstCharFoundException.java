package exception;

public class NoFirstCharFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoFirstCharFoundException() {
		super("Todos os elementos do array repetem.");
	}
}
