package exception;

public class AlreadyExistCepException extends Exception {

	private static final long serialVersionUID = 1L;

	public AlreadyExistCepException() {
		super("Nao e possivel fazer a inclusao, pois o cep j� existe na base.");
	}

}
