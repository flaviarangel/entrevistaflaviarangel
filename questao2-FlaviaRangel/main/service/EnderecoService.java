package service;

import dao.EnderecoDAO;
import entity.EnderecoEntity;
import exception.AlreadyExistCepException;
import exception.NoValidCepException;
import exception.NonExistentValidCepException;
import vo.EnderecoVO;

public class EnderecoService {

	private final int CEP_LENGHT = 8;

	private final int STOP_REPLACING_POSITION = -1;

	private int replacePosition = CEP_LENGHT + STOP_REPLACING_POSITION;

	private EnderecoDAO dao = new EnderecoDAO();

	public void insert(EnderecoVO vo) throws AlreadyExistCepException {
		dao.insert(new EnderecoEntity(vo.getRua(), vo.getNumero(), vo.getComplemento(), vo.getBairro(),
				vo.getCidade(), vo.getEstado(), vo.getCep()));
	}

	public void update(EnderecoVO vo) {
		EnderecoEntity entity = dao.findById(vo.getId());

		entity.setRua(vo.getRua());
		entity.setNumero(vo.getNumero());
		entity.setComplemento(vo.getComplemento());
		entity.setBairro(vo.getBairro());
		entity.setCidade(vo.getCidade());
		entity.setEstado(vo.getEstado());
		entity.setCep(vo.getCep());

		dao.update(entity);
	}

	public void delete(EnderecoVO vo) {
		dao.delete(vo.getId());
	}

	public EnderecoVO getEnderecoByCEP(String cep) throws NoValidCepException, NonExistentValidCepException {

		String cleanCep = getCleanCEP(cep);

		if (cleanCep.length() != CEP_LENGHT) {
			throw new NoValidCepException();
		}

		String validCep = cleanCep;

		while (replacePosition > STOP_REPLACING_POSITION) {

			EnderecoEntity entity = dao.findByCEP(validCep);

			if (entity != null) {
				return new EnderecoVO(entity);
			}

			validCep = replaceDigitForZeroFromRightToLeft(validCep);
			replacePosition--;
		}

		if (replacePosition == STOP_REPLACING_POSITION)
			throw new NonExistentValidCepException();

		return null;
	}

	private String getCleanCEP(String cep) {

		String onlyDigitsCEP = "";
		char[] characteres = cep.toCharArray();

		for (char character : characteres) {
			if (Character.isDigit(character)) {
				onlyDigitsCEP = onlyDigitsCEP + character;
			}
		}

		return onlyDigitsCEP;
	}

	private String replaceDigitForZeroFromRightToLeft(String cep) {

		StringBuilder newCEP = new StringBuilder(cep);
		newCEP.setCharAt(replacePosition, '0');

		return newCEP.toString();
	}

}
