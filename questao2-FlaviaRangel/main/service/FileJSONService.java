package service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileJSONService {

	public String readFileJSON(String fileName) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(fileName));

		String line = br.readLine();

		br.close();

		return line;
	}
}
