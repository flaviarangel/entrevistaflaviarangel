package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entity.EnderecoEntity;
import exception.AlreadyExistCepException;

public class EnderecoDAO {

	private EntityManagerFactory emf;
	private EntityManager em;

	public void startConnection() {
		emf = Persistence.createEntityManagerFactory("entrevistaDB");
		em = emf.createEntityManager();
		em.getTransaction().begin();
	}

	public void closeConnection() {
		em.getTransaction().commit();
		emf.close();
		em.close();
	}

	public void insert(EnderecoEntity entity) throws AlreadyExistCepException {
		if (findByCEP(entity.getCep()) != null)
			throw new AlreadyExistCepException();

		em.persist(entity);
	}

	public void update(EnderecoEntity entity) {
		em.merge(entity);
	}

	public void delete(Integer id) {
		EnderecoEntity entity = findById(id);
		em.remove(entity);
	}

	public EnderecoEntity findById(Integer id) {
		return em.find(EnderecoEntity.class, id);
	}

	public EnderecoEntity findByCEP(String cep) {

		final StringBuilder hql = new StringBuilder();
		hql.append("SELECT e FROM Endereco e WHERE ");
		hql.append(" cep = :cep ");

		final Query query = em.createQuery(hql.toString());
		query.setParameter("cep", cep);

		return (EnderecoEntity) query.getSingleResult();
	}

}
