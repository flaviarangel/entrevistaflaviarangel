Quest�o 2

O seguinte projeto se diferencia da quest�o 1 no mapeamento da classe EnderecoEntity com as anota��es do Hibernate e na implementa��o dos m�todos insert, update, findById e delete da classe EnderecoDAO. O mapeamento da entity serve para identificar essa classe como sendo representativa da tabela Endereco do banco e o DAO serve para efetuar cada a��o no banco de dados mySQL.
Vale ressaltar que, ao implementar as a��es de CRUD no DAO fazemos com que a regra de neg�cio (l�gica) no service tem a liberdade de ser alterar sem prejudicar e impactar as a��es que s�o feitas diretamente no banco de dados (baixo acoplamento).
Apesar das modifica��es n�o tive tempo de fazer a conex�o com o banco de dados mySQL funcionar.
