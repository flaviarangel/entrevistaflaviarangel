package servicetest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import dao.EnderecoDAO;
import entity.EnderecoEntity;
import exception.NoValidCepException;
import exception.NonExistentValidCepException;
import mock.EnderecoMock;
import service.EnderecoService;
import vo.EnderecoVO;

import static org.mockito.Mockito.when;

public class EnderecoServiceTest {

	@Mock
	private EnderecoDAO dao = new EnderecoDAO();
	
	private EnderecoMock mock = new EnderecoMock();

	@InjectMocks
	private EnderecoService service = new EnderecoService();
	
	@Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test(expected = NoValidCepException.class)
	public void testCepStringWithOnlyCharacters() throws NoValidCepException, NonExistentValidCepException {
		service.getEnderecoByCEP("abcdefgh");
	}

	@Test(expected = NoValidCepException.class)
	public void testCepStringWithLenghtDifferentOfEight() throws NoValidCepException, NonExistentValidCepException {
		service.getEnderecoByCEP("123456-789");
	}
	
	@Test
	public void testCepValidWithCepStringMadeByDigitsAndCharacters() throws NoValidCepException, NonExistentValidCepException {
		testCepValid("3.5,5/0&0%2a4c5#qt*a)s", mock.getAddress1());
	}
	
	@Test
	public void testCepValidWithOneZeroOnTheRight() throws NoValidCepException, NonExistentValidCepException {
		testCepValid("11436798", mock.getAddress2());
	}
	
	@Test
	public void testCepValidWithThreeZerosOnTheRight() throws NoValidCepException, NonExistentValidCepException {
		testCepValid("12837534", mock.getAddress3());
	}
	
	@Test(expected = NonExistentValidCepException.class)
	public void testCepNonexistent() throws NoValidCepException, NonExistentValidCepException {
		service.getEnderecoByCEP("05405100");
	}
	
	private void testCepValid(String cep, EnderecoEntity entity) throws NoValidCepException, NonExistentValidCepException{
		when(dao.findByCEP(Mockito.anyString())).thenReturn(entity);
		
		EnderecoVO atual = service.getEnderecoByCEP(cep);		
		EnderecoVO vo = new EnderecoVO(entity);
		Assert.assertEquals("O cep retornou o endereco: "+ atual.toString()+
				" - O qual � diferente do endereco esperado: "+ vo.toString(), vo, atual);
	}

}
