package mock;

import java.util.ArrayList;
import java.util.List;

import entity.EnderecoEntity;

public class EnderecoMock {

	private List<EnderecoEntity> listaEnderecos = new ArrayList<EnderecoEntity>();;

	public void persist(EnderecoEntity entity){
		listaEnderecos.add(entity);
	}
	
	public List<EnderecoEntity> createListOfEnderecoEntity() {
		
		listaEnderecos.add(getAddress1());
		listaEnderecos.add(getAddress2());
		listaEnderecos.add(getAddress3());

		return listaEnderecos;
	}

	public EnderecoEntity getAddress1() {
		return new EnderecoEntity("Rua Estanho", "Sao Joao de Deus", "Divinopolis", "MG", "35500245");
	}

	public EnderecoEntity getAddress2() {
		return new EnderecoEntity("Rua Viriato Correia", "Fortaleza", "Blumenau", "SC", "11436790");
	}

	public EnderecoEntity getAddress3() {
		return new EnderecoEntity("Rua Fe em Deus", "	Vila Lobao", "	Sao Luis", "MA", "12837000");
	}

	/*
	 * public EnderecoEntity getAddress4(){ return new EnderecoEntity(
	 * "Rua Arino Sai Leitao", "Joquei Clube", "Sao Goncalo", "RJ", "24743780");
	 * }
	 * 
	 * public EnderecoEntity getAddress5(){ return new EnderecoEntity(
	 * "Avenida Adhemar de Barros", "Vila Ligya", "Guaruja", "SP", "11430005");
	 * } public EnderecoEntity getAddress6(){ return new EnderecoEntity(
	 * "Rua Colantonio", "	Vila Andrade", "Sao Paulo", "SP", "40090023"); }
	 */
}
