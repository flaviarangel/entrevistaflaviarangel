package dao;

import java.util.List;

import entity.EnderecoEntity;
import mock.EnderecoMock;

public class EnderecoDAO {

	private EnderecoMock mock = new EnderecoMock();

	public EnderecoEntity findByCEP(String cep) {

		List<EnderecoEntity> listaEnderecos = mock.createListOfEnderecoEntity();

		for (EnderecoEntity endEntity : listaEnderecos) {
			if (endEntity.getCep().equals(cep)) {
				return endEntity;
			}
		}
		return null;
	}

}
