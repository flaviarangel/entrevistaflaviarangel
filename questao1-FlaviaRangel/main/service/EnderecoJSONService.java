package service;

import com.google.gson.Gson;

import exception.NoValidCepException;
import exception.NonExistentValidCepException;
import vo.EnderecoVO;
import vo.ErrorVO;

public class EnderecoJSONService {

	private Gson gson = new Gson();

	private EnderecoService enderecoService = new EnderecoService();

	public String getEnderecoByCEP(String json) {
		EnderecoVO vo = null;

		try {
			vo = enderecoService.getEnderecoByCEP(JSONtoCEP(json));
		} catch (NoValidCepException noValidCep) {
			return errorVOtoJSON(new ErrorVO(noValidCep.getMessage()));
		} catch (NonExistentValidCepException nonExistentValidCep) {
			return errorVOtoJSON(new ErrorVO(nonExistentValidCep.getMessage()));
		}

		return enderevoVOtoJSON(vo);
	}

	private String JSONtoCEP(String json) {
		EnderecoVO vo = gson.fromJson(json, EnderecoVO.class);
		return vo.getCep();
	}

	private String enderevoVOtoJSON(EnderecoVO vo) {
		return gson.toJson(vo);
	}

	public String errorVOtoJSON(ErrorVO error) {
		return gson.toJson(error);
	}
}
