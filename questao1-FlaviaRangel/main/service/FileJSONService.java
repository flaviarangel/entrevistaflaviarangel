package service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileJSONService {

	public String readFileJSON(String filePath) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(filePath));

		String line = br.readLine();

		br.close();

		return line;
	}
}
