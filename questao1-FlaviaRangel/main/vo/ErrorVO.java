package vo;

import com.google.gson.annotations.SerializedName;

public class ErrorVO {

	@SerializedName("msg")
	private final String message;

	public ErrorVO(String message) {
		this.message = message;
	}
}
