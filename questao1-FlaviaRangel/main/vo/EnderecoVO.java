package vo;

import java.util.Objects;

import entity.EnderecoEntity;

public class EnderecoVO {

	private final String rua;
	private final String bairro;
	private final String cidade;
	private final String estado;
	private final String cep;

	public EnderecoVO(String rua, String bairro, String cidade, String estado, String cep) {
		this.rua = rua;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
		this.cep = cep;
	}

	public EnderecoVO(EnderecoEntity entity) {
		this.rua = entity.getRua();
		this.bairro = entity.getBairro();
		this.cidade = entity.getCidade();
		this.estado = entity.getEstado();
		this.cep = entity.getCep();
	}

	public String getRua() {
		return rua;
	}

	public String getBairro() {
		return bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public String getEstado() {
		return estado;
	}

	public String getCep() {
		return cep;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		final EnderecoVO that = (EnderecoVO) o;
		return Objects.equals(rua, that.rua) //
				&& Objects.equals(bairro, that.bairro) //
				&& Objects.equals(cidade, that.cidade) //
				&& Objects.equals(estado, that.estado) //
				&& Objects.equals(cep, that.cep);
	}

}
