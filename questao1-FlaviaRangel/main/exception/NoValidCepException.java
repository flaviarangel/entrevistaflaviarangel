package exception;

public class NoValidCepException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoValidCepException() {
		super("CEP Inv�lido");
	}

}
