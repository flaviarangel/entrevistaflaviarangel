package exception;

public class NonExistentValidCepException extends Exception {

	private static final long serialVersionUID = 1L;

	public NonExistentValidCepException() {
		super("O CEP informado n�o existe na base de dados");
	}

}
