package servicetest;

import service.EnderecoJSONService;
import service.EnderecoService;
import service.FileJSONService;
import vo.EnderecoVO;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import exception.NoValidCepException;
import exception.NonExistentValidCepException;
import mock.EnderecoMock;

import static org.mockito.Mockito.when;

public class EnderecoJSONServiceTest {

	private EnderecoMock mock = new EnderecoMock();

	private FileJSONService fileJsonService = new FileJSONService();

	@Mock
	private EnderecoService enderecoService = new EnderecoService();

	@InjectMocks
	private EnderecoJSONService enderecoJSONService = new EnderecoJSONService();

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testJsonOutFile() throws NoValidCepException, NonExistentValidCepException, IOException {

		EnderecoVO enderecoVO = new EnderecoVO(mock.getAddress1());

		String jsonIn = fileJsonService.readFileJSON("./resource/jsonIn1.json");
		String jsonExpectedOut = fileJsonService.readFileJSON("./resource/jsonOut1.json");

		when(enderecoService.getEnderecoByCEP(Mockito.anyString())).thenReturn(enderecoVO);

		String jsonOut = enderecoJSONService.getEnderecoByCEP(jsonIn);

		Assert.assertEquals(
				"O arquivo json de saida: " + jsonOut + " nao condiz com o json esperado: " + jsonExpectedOut,
				jsonExpectedOut, jsonOut);
	}

	@Test
	public void testJsonOutErrorFileWhenNoValidCepException()
			throws NoValidCepException, NonExistentValidCepException, IOException {

		String jsonIn = fileJsonService.readFileJSON("./resource/jsonIn1.json");
		String jsonExpectedOut = fileJsonService.readFileJSON("./resource/jsonOutNoValidCepException.json");

		when(enderecoService.getEnderecoByCEP(Mockito.anyString())).thenThrow(new NoValidCepException());

		String jsonOut = enderecoJSONService.getEnderecoByCEP(jsonIn);

		Assert.assertEquals(
				"O arquivo json de saida: " + jsonOut + " nao condiz com o json esperado: " + jsonExpectedOut,
				jsonExpectedOut, jsonOut);

	}

	@Test
	public void testJsonOutErrorFileWhenNonExistentValidCepException()
			throws NoValidCepException, NonExistentValidCepException, IOException {

		String jsonIn = fileJsonService.readFileJSON("./resource/jsonIn1.json");
		String jsonExpectedOut = fileJsonService.readFileJSON("./resource/jsonOutNonExistentCepException.json");

		when(enderecoService.getEnderecoByCEP(Mockito.anyString())).thenThrow(new NonExistentValidCepException());

		String jsonOut = enderecoJSONService.getEnderecoByCEP(jsonIn);

		Assert.assertEquals(
				"O arquivo json de saida: " + jsonOut + " nao condiz com o json esperado: " + jsonExpectedOut,
				jsonExpectedOut, jsonOut);
	}

}
