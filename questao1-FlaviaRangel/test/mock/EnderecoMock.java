package mock;

import java.util.ArrayList;
import java.util.List;

import entity.EnderecoEntity;

public class EnderecoMock {

	public List<EnderecoEntity> createListOfEnderecoEntity() {

		List<EnderecoEntity> listaEnderecos = new ArrayList<EnderecoEntity>();

		listaEnderecos.add(getAddress1());
		listaEnderecos.add(getAddress2());
		listaEnderecos.add(getAddress3());

		return listaEnderecos;
	}

	public EnderecoEntity getAddress1() {
		return new EnderecoEntity("Rua Estanho", "Sao Joao de Deus", "Divinopolis", "MG", "35500245");
	}

	public EnderecoEntity getAddress2() {
		return new EnderecoEntity("Rua Viriato Correia", "Fortaleza", "Blumenau", "SC", "11436790");
	}

	public EnderecoEntity getAddress3() {
		return new EnderecoEntity("Rua Fe em Deus", "	Vila Lobao", "	Sao Luis", "MA", "12837000");
	}

}
