Questao 1

A estrutura do projeto ficou da seguinte maneira: 

main     -> dao         -> EnderecoDAO.java
	 -> exception   -> NonExistentValidCepException.java
                        -> NoValidCepExcpetion.java
         -> entity      -> EnderecoEntity.java
         -> service     -> EnderecoJSONService.java
                        -> EnderecoService.java
                        -> FileJSONService.java
         -> vo          -> EnderecoVO.java
                        -> ErrorVO.java

test     -> mock        -> EnderecoMock.java
         -> servicetest -> EnderecoJSONServiceTest.java
                        -> EnderecoServiceTest.java

resource -> jsonIn1.json
         -> jsonOut1.json
         -> jsonOutNonExistentCepException.json
         -> jsonOutNoValidCepExcpetion.json

Onde a pasta "main" contém a aplicação em si, a pasta "test" contém todos os testes unitários e a pasta "resource" contém todos os arquivos .json usado nos testes da aplicação.

Com relação a aplicação (pasta main), essa está dividida no pacotes:
1) entity: Contém a classe que representa a estrutura do dado a ser manipulado. No caso de uso de banco de dados, essa seria a classe representativa do modelo do banco, a qual seria mapeada com o Hybernate.  
2) dao: Contém as classes que fazem a manipulação dos dados. No caso de uso de banco de dados, essas classes mapeariam os as funções insert, update, find e delete. Para esse exercício, a classe EnderecoDAO tem o método findByCEP, o qual procura um cep específico em uma lista de enderecos mockados.
3) service: Contém as classes que manipulam as regras de negócio(lógica) da aplicação. 
* A classe EnderecoService tem o método getEnderecoByCep recebendo uma String de cep. Primeiramente, é formatado a String cep para que não tenha nenhum caracter que não sejam números. Então verifica se esse cep informado tem o tamanho esperado. 
   Se não, tem lança uma exceção chamada NoValidCepException, que comprova que o cep informado não é válido.
   Se sim, procura o CEP na base de dados. 
      Se o dado foi encontrado, ele é retornado na estrutura de um EnderecoVO.
      Se o dado não for encontrado, é substituído o último zero a direita da String de cep e novamente procura na base.
Caso tenha substituído todos os digitados do cep por zero e mesmo assim não tenha encontrado o cep na base, então é lançada uma exceção chamada NonExistentValidCepException, que alerta que esse cep não existe na base de dados.
* A classe EnderecoJSONService tem o método getEnderecoByCep que recebe uma String no formato json e também responde uma String no formato json. Esse método chama o EnderecoService.getEnderecoByCep depois transformar o json recebido em String. E ao receber o resultado, converte esse para uma String em formato json. 
IMPORTANTE: Todas as manipulações "de e para" json foram feitas utilizando a biblioteca do google chamada Gson (com.google.gson.Gson).
*A classe FileJSONService tem o método readFileJSON recebendo como atributo o path do arquivo. Nesse método é feita a leitura do conteúdo de um arquivo físico e é retornado esse conteúdo em formato de String.
4) vo: Contém as classes que representam os dados manipuláveis de forma serializável. Diferente da entity que reflete o modelo do banco de dados, as classes VO podem ser manipuladas pelos services e podem ser usadas em classes de Front-end onde são mapeados os métodos HTTP(GET e POST), por exemplo. 
5) exception: Contém as classes que representam exceções da aplicação.

A pasta "test" está organizada da seguinte maneira:
1) mock: Contém a classe que representa a "base de dados" do sistema. 
2) servicetest: Contém as classes de testes unitários. 
A classe EnderecoJSONServiceTest testa todas as possibilidades de entrada e reposta de um arquivo do tipo .json. Os cenários são:
  * testJsonOutFile ("fluxo feliz")
  * testJsonOutErrorFileWhenNoValidCepException: testa o arquivo json de saída quando a aplicação retorna NoValidCepException.
  * testJsonOutErrorFileWhenNonExistentValidCepException: testa o arquivo json de saída quando a aplicação retorna NonExistentValidCepException.
A classeEnderecoServiceTest testa todas as possibilidades de entrada de cep e a resposta da aplicação em caso de cep inválido, válido e válido com substituição de zero a direita. Os cenários são:
  * testCepStringWithOnlyCharacters : quando cep informado tem somente caracteres
  * testCepStringWithLenghtDifferentOfEight : quando o cep informado tem tamanho diferente de oito
  * testCepValidWithCepStringMadeByDigitsAndCharacters: quando cep informado tem digitos e caracteres.
  * testCepValidWithOneZeroOnTheRight: quando é preciso substituir 1 caracter a direita por zero
  * testCepValidWithThreeZerosOnTheRight: quando é preciso substituir 3 caracteres a direita por zero
  * testCepNonexistent: quando o cep informado não existe na base de dados
IMPORTANTE: Todos os mocks de serviços feitos nos testes unitários foram feitos utilizando a biblioteca do Mockito.

